<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ball;
use App\Models\Bucket;
use App\Models\Result;

class GameController extends Controller
{
    public function index(Request $request){
        $balls = Ball::all();
        $buckets = Bucket::all();
        $result = Result::first();
        return view('game',compact("balls", "buckets", "result"));
    }

    public function play(Request $request){
        // dd($request);
        // Retrieve the input data from the request
        $inputData = $request->validate([
            'balls.*' => 'required|numeric',
        ]);
        // var_dump($inputData);

        // Update all balls from the database
        foreach($inputData['balls'] as $color=>$volume){
            $ball = Ball::where('name', $color)->first();
            $ball->pick = $volume;
            $ball->save();
        }
        
        // Retrieve all buckets from the database
        $buckets = Bucket::all();

        // Retrieve all balls from the database
        $balls = Ball::all();

        $result = Result::first();
        if(empty($result) || empty($result->fill_bucket)){
            $fill_bucket=[];

            // Create empty buckets
            foreach($buckets as $val){
                $temp_ball = [];
                foreach($balls as $val1){
                    $temp_ball[$val1->name]=0;
                }
                $fill_bucket[$val->name] = $temp_ball;
            }
        }else{
            $fill_bucket = json_decode($result->fill_bucket,true);
        }

        
        // Remaing ball
        $remaing_ball=[];
        foreach($balls as $ball){
            $remaing_ball[$ball->name] = 0;
        }
        // dd($fill_bucket);

        $full=0;

        // Loop start in given ball
        foreach($balls as $ball){
            if($ball->pick==0){
                continue;
            }
            // Loop start buckets
            foreach ($buckets as $key=>$bucket) {
                // Check bucket full or not
                if($bucket->current_vol == $bucket->volume){
                    continue;
                }
                // Claculate empty bucket
                $empty_vol = $bucket->volume - $bucket->current_vol;
                $totall_ball = (int) ($empty_vol/$ball->volume);
                // Check ball volume and empty volume
                if($empty_vol >= $ball->volume){
                    $full=1;
                    if($ball->pick>=$totall_ball){
                        // fill bucket
                        $fill_bucket[$bucket->name][$ball->name]+=$totall_ball;
                        $buckets[$key]->current_vol+=($totall_ball*$ball->volume);
                        $buckets[$key]->save();
                        if($ball->pick>$totall_ball){
                            $ball->pick-=$totall_ball;
                            $ball->save();
                            continue;
                        }
                        $ball->pick=0;
                        $ball->save();
                        break;
                    }else if($ball->pick<$totall_ball){
                        $fill_bucket[$bucket->name][$ball->name]+=$ball->pick;
                        $buckets[$key]->current_vol+=($ball->pick*$ball->volume);
                        $ball->pick=0;
                        $ball->save();
                        $buckets[$key]->save();
                        break;
                    }
                }

            }
        }
        // $buckets->save();
        
        if(empty($result)){
            Result::create(["fill_bucket"=>json_encode($fill_bucket)]);
        }else{
            Result::where("id",1)->update(["fill_bucket"=>json_encode($fill_bucket)]);
        }
        
        return redirect(url("/"));
        
        // dump($fill_bucket,$buckets);
        // return view('game-play',compact("balls","buckets","fill_bucket"));
        
    }

    public function restart(){
        Ball::query()->update(["pick"=>0]);
        Bucket::query()->update(["current_vol"=>0]);
        Result::query()->update(["fill_bucket"=>null]);
        return redirect(url("/"));
    }

    public function create_ball(Request $request){
        $validated = $request->validate([
            'ball_name' => 'required|unique:balls,name|max:255',
            'ball_volume' => 'required|decimal:0,1',
        ]);

        $bucket = Ball::create([
            "name"=>$request->ball_name,
            "volume"=>$request->ball_volume
        ]);
        return back()->with('success', 'Ball Create Successfully.');
    }

    public function create_bucket(Request $request){
        $validated = $request->validate([
            'bucket_name' => 'required|unique:buckets,name|max:255',
            'bucket_volume' => 'required|decimal:0,1',
        ]);
        $bucket = Bucket::create([
            "name"=>$request->bucket_name,
            "volume"=>$request->bucket_volume
        ]);
        return back()->with('success', 'Bucket Create Successfully.');
    }
}

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <ul>
                        <li>{!! \Session::get('success') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header">{{ __('Bucket Form') }}</div>
    
                    <div class="card-body">
                        <form action="{{ route('bucket') }}" method="POST">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Bucket Name</label>
                                <input name="bucket_name" type="text" class="form-control @error('bucket_name') is-invalid @enderror" value="{{ old('bucket_name') }}">
                                @error('bucket_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Volume (in inches)</label>
                                <input name="bucket_volume" type="text" class="form-control @error('bucket_volume') is-invalid @enderror" value="{{ old('bucket_volume') }}">
                                @error('bucket_volume')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-warning">SAVE</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-header">{{ __('Ball Form') }}</div>
    
                    <div class="card-body">
                        <form action="{{ route('ball') }}" method="POST">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Ball Name</label>
                                <input name="ball_name" type="text" class="form-control @error('ball_name') is-invalid @enderror"  value="{{ old('ball_name') }}">
                                @error('ball_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label>Volume (in inches)</label>
                                <input name="ball_volume" type="text"   class="form-control @error('ball_volume') is-invalid @enderror"  value="{{ old('ball_volume') }}">
                                @error('ball_volume')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <button type="submit" class="btn btn-warning">SAVE</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>

            @if (!empty($balls))
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-header">{{ __('Bucket Suggestion') }}</div>
    
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 mb-3 mb-md-0">
                                <form action="{{ route('play') }}" method="POST">
                                    @csrf
                                        @foreach ($balls as $item)
                                        <div class="form-group mb-3">
                                            <label>{{ $item->name }}</label>
                                            <input type="text" name="balls[{{ $item->name }}]" class="form-control"> 
                                        </div>
                                        @endforeach
                                        <div class="form-group mb-3">
                                            <button type="submit" class="btn btn-warning">SAVE</button>
                                            <a href="{{ route('restart') }}" class="btn btn-info">Restart</a>
                                        </div>
                                </form>
                            </div>
                            <div class="col-md-6">
                                <h4>Result</h4>
                                <p>Following are the Suggested buckets:</p>
                                <ul>
                                @foreach($buckets as $bucket)
                                    <li>Bucket {{ $bucket->name }} : <b>Place 
                                        @foreach($fill_bucket[$bucket->name] as $key=>$val)
                                            @if($val!=0)
                                                {{ $val." ".$key }},
                                            @endif
                                        @endforeach
                                    </b>
                                @endforeach
                                </ul>

                                <p>Remaing Ball</p>
                                <ul>
                                @foreach($balls as $ball)
                                    @if($ball->pick!=0)
                                        <li>{{ $ball->pick." ".$ball->name }}</li>
                                    @endif
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GameController::class, 'index']);

Route::post("/ball",[GameController::class, 'create_ball'])->name("ball");
Route::post("/bucket",[GameController::class, 'create_bucket'])->name("bucket");
Route::post("/play",[GameController::class, 'play'])->name("play");
Route::get("/restart",[GameController::class, 'restart'])->name("restart");

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
